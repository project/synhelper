<?php
/**
 * @file
 * Template file used to generate .phpstorm.meta.php.
 *
 * @var array $serviceMap
 *   Service map.
 * @var array $storageMap
 *   Storage map.
 */
?>

<?php echo "<?php" ?>

namespace PHPSTORM_META {

  override(\Drupal::service(0),
  map([<?php foreach ($serviceMap as $serviceId => $serviceClass): ?>
    <?php echo "'$serviceId' =>  $serviceClass," . PHP_EOL; ?>
  <?php endforeach; ?>]));

  override(\Symfony\Component\DependencyInjection\ContainerInterface::get(0),
  map([<?php foreach ($serviceMap as $serviceId => $serviceClass): ?>
    <?php echo "'$serviceId' =>  $serviceClass," . PHP_EOL;?>
  <?php endforeach; ?>]));

  override(\Drupal::entityTypeManager()->getStorage(''),
  map([<?php foreach ($storageMap as $storageId => $storageClass): ?>
    <?php echo "'$storageId' =>  $storageClass," . PHP_EOL;?>
  <?php endforeach; ?>]));

}
