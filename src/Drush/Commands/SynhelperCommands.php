<?php

namespace Drupal\synhelper\Drush\Commands;

use Drupal\Core\Config\FileStorage;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
final class SynhelperCommands extends DrushCommands {

  /**
   * The service container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * SynhelperCommands constructor.
   */
  public function __construct() {
    parent::__construct();
    $this->container = \Drupal::getContainer();
    $this->entityTypeManager = \Drupal::entityTypeManager();
  }

  /**
   * Generate PHPStorm Metadata.
   */
  #[CLI\Command(name: 'synhelper:phpstorm')]
  #[CLI\Usage(name: 'synhelper:phpstorm', description: 'Generate PHPStorm Metadata.')]
  public function generateMetadata() {
    try {
      $this->logger()->notice('Generating PHPStorm Metadata file');
      $file = $this->generate();
      file_put_contents(DRUPAL_ROOT . '/.phpstorm.meta.php', $file);
      $this->logger()->success('Done!');
    }
    catch (\Exception $e) {
      $this->logger()
        ->error("An error occurred during file generation: @error", ['@error' => $e->getMessage()]);
    }
  }

  /**
   * Vacuum a sqlite database https://git.drupalcode.org/project/sqlite_vacuum.
   */
  #[CLI\Command(name: 'synhelper:sqlite-vacuum')]
  #[CLI\Usage(name: 'synhelper:sqlite-vacuum', description: 'Vacuum a sqlite database')]
  public function vacuum($target = 'default', $key = NULL) {
    $connectipn = Database::getConnection($target, $key);
    if ($connectipn->databaseType() === 'sqlite') {
      $connectipn->query('vacuum;');
    } else {
      $this->logger()->error(dt('sql:vacuum works only on sqlite databases.'));
    }
  }

  /**
   * Generate PHPStorm metadata file contents.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function generate() {
    $this->logger()->notice('getServiceMap ...');
    $serviceMap = $this->getServiceMap();
    $this->logger()->notice('getStorageMap ...');
    $storageMap = $this->getStorageMap();
    return $this->generateFileContents($serviceMap, $storageMap);
  }

  /**
   * Generate PHPStorm metadata file contents from provided services/storages.
   *
   * @param array $serviceMap
   *   Service map.
   * @param array $storageMap
   *   Storage map.
   *
   * @return string
   *   PHPStorm metadata file contents.
   */
  private function generateFileContents(array $serviceMap, array $storageMap) {
    ob_start();
    include __DIR__ . '/.phpstorm.meta.TEMPLATE.php';
    return ob_get_clean();
  }

  /**
   * Get a map of services class names keyed by service name.
   *
   * @return array
   *   Map of services class names keyed by service name.
   */
  private function getServiceMap() {
    $serviceMap = [];
    foreach ($this->container->getServiceIds() as $serviceId) {
      try {
        $service = $this->container->get($serviceId);
      }
      catch (\Throwable $e) {
        $this->logger()->error("$serviceId ::: " . $e->getMessage());
      }
      // Skip non-existing service classes and private services.
      if (is_object($service) && strpos($serviceId, 'private__') !== 0) {
        $serviceMap[$serviceId] = '\\' . get_class($service) . '::class';
      }
    }
    return $serviceMap;
  }

  /**
   * Get a map of storage class names keyed by entity name.
   *
   * @return array
   *   Map of storage class names keyed by entity name.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getStorageMap() {
    $storageMap = [];
    foreach ($this->entityTypeManager->getDefinitions() as $type => $definition) {
      $class = $this->entityTypeManager->getStorage($type);
      $storageMap[$type] = '\\' . get_class($class) . '::class';
    }
    return $storageMap;
  }

  /**
   * Synhelper config import.
   */
  #[CLI\Command(name: 'synhelper:cim', aliases: ['syncim'])]
  #[CLI\Argument(name: 'directory', description: 'Directory of config files.')]
  #[CLI\Option(name: 'arr', description:  'An option that takes multiple values.')]
  #[CLI\Usage(name: 'synhelper:cim /var/www/config', description: 'Import config from  /var/www/config.')]
  public function configImport($directory) {
    try {
      $this->output()->writeln("Import config from: $directory");
      $source = new FileStorage($directory);
      \Drupal::service('config.installer')->installOptionalConfig($source);
      foreach (scandir($directory) as $file) {
        $cfg = substr($file, 0, -4);
        if ($cfg) {
          $this->output()->writeln("Config: $file");
          $data = $source->read($cfg);
          $config = \Drupal::service('config.factory')->getEditable($cfg);
          $config->setData($data)->save();
        }
      }
      $this->logger()->success(dt('Done Import.'));
    }
    catch (\Exception $e) {
      $this->logger()->error("An error occurred during config import: @error", ['@error' => $e->getMessage()]);
    }
  }

}
