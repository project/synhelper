<?php

namespace Drupal\synhelper\Hook;

/**
 * SetMessageTitle.
 */
class ContactMessagePresave {

  /**
   * Set Title.
   */
  public static function setTitle($entity) {
    if (empty($entity->subject->getValue())) {
      $formId = $entity->contact_form->getString();
      $entityForm = \Drupal::service('entity_type.manager')->getStorage('contact_form')->load($formId);
      $formTitle = !empty($entityForm->get('label')) ? $entityForm->get('label') : t('Contact form');
      $formTitle .= " - " . \Drupal::service('date.formatter')->format(\Drupal::time()->getRequestTime(), 'custom', 'dM H:i:s');
      $entity->subject->setValue($formTitle);
    }
  }

}
