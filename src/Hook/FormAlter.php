<?php

namespace Drupal\synhelper\Hook;

use Drupal\Core\Url;

/**
 * RegisterContactForm.
 */
class FormAlter {

  /**
   * Hook.
   */
  public static function hook(&$form, &$form_state, $form_id) {

    $config = \Drupal::config('synhelper.settings');

    // FZ-152 checkbox.
    if ($config->get('fz152') && strpos($form_id, 'user_register_form') === 0) {
      $url = Url::fromUserInput('/policy');
      $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $text = [
        'title' => t("I consent to the processing of personal data"),
        'description' => t(
          "<a href='@href' target='_blank'>Cookie & Privacy Policy for Website</a>",
          ['@href' => $url->toString()]
        ),
      ];
      if ($lang == 'ru') {
        $text = [
          'title' => "Нажимая кнопку, я даю согласие на&nbsp;обработку <a href='/policy' target='_blank'>персональных данных</a>",
          'description' => "",
        ];
      }
      $form['fz152_agreement'] = [
        '#type' => 'checkbox',
        '#title' => $text['title'],
        '#default_value' => FALSE,
        '#required' => TRUE,
        // HTML5 support.
        '#attributes' => [
          'required' => 'required',
          'class' => ['form-item--fz-152-checkbox'],
        ],
        '#wrapper_attributes' => [
          'class' => 'form-item--fz-152',
        ],
        '#label_attributes' => [
          'class' => 'form-item--fz-152-label',
        ],
        '#description' => [
          '#type' => 'markup',
          '#prefix' => '<div class="form-item--fz-152-desctiprion">',
          '#suffix' => '</div>',
          '#markup' => $text['description'],
        ],
        '#weight' => 99,
      ];
    };
  }
}
