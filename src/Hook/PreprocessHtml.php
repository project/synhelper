<?php

namespace Drupal\synhelper\Hook;

use Drupal\Core\Render\Markup;
use Drupal\Core\Url;

/**
 * Preprocess Html.
 */
class PreprocessHtml {

  /**
   * Hook.
   */
  public static function hook(&$variables) {
    // No-index:
    $noindex_paths = [
      '/user/login',
      '/user/password',
      '/policy',
      '/policy/ru',
      '/policy/en',
    ];

    $noindex = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'robots',
        'content' => 'none',
      ],
    ];

    $current_path = \Drupal::service('path.current')->getPath();
    if (in_array($current_path, $noindex_paths)) {
      $variables['page']['#attached']['html_head'][] = [$noindex, 'indexation'];
    };
    $config = \Drupal::config('synhelper.settings');
    if ($config->get('no-index')) {
      $variables['page']['#attached']['html_head'][] = [$noindex, 'indexation'];
    }
    $host = \Drupal::request()->getHost();
    if ($config->get('no-index-1c') && strpos($host, '1c.') === 0) {
      $variables['page']['#attached']['html_head'][] = [$noindex, 'indexation'];
    }
  }

}
