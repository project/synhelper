<?php

namespace Drupal\synhelper\Hook;

use Algo26\IdnaConvert\ToIdn;

/**
 * PreprocessHtml.
 */
class PhpmailAlterFromAlter {

  /**
   * Hook.
   */
  public static function hook(&$mail) {
    $idna = new ToIdn();
    // $unicode = new Algo26\IdnaConvert\ToUnicode();
    $mail = trim($mail);
    $user = strstr($mail, '@', TRUE);
    $domain = strstr($mail, '@');
    $domain = substr($domain, 1);
    if (strpos($domain, '>')) {
      $domain = str_replace('>', '', $domain);
      $domain = $idna->convert($domain) . '>';
    }
    else {
      $domain = $idna->convert($domain);
    }
    $mail = "{$user}@{$domain}";
  }

}
