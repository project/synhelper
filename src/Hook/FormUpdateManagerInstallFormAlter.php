<?php

namespace Drupal\synhelper\Hook;

/**
 * Form Update Manager Install FormA lter.
 */
class FormUpdateManagerInstallFormAlter {

  /**
   * Hook.
   */
  public static function hook(&$form, &$form_state, $form_id) {
    unset($form['project_url']);
    unset($form['information']);
    unset($form['project_upload']);
    unset($form['form_token']);
    unset($form['actions']);
    unset($form['#submit']);
    $form['#prefix'] = "<h3>Access denied</h3>";
    $form['#prefix'] .= "<p>Use <code>composer require drupal/MODULE_NAME</code> in drupal root directory.</p>";
  }

}
