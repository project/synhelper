<?php

namespace Drupal\synhelper\Hook;

/**
 * PreprocessPage.
 */
class PreprocessPage {

  /**
   * Hook.
   */
  public static function hook(&$variables) {
    $config = \Drupal::config('synhelper.settings');
    $config_synapse = \Drupal::config('synapse.settings');
    if ($config->get('fz152')) {
      $variables['footer_policy'] = TRUE;
    }
    $metrika = [
      'ya_counter' => $config->get('ya-counter') ?? '',
      'gtm' => $config_synapse->get('gtm-id') ?? '',
      'ga4' => $config_synapse->get('ga4-id') ?? '',
    ];
    $variables['#attached']['drupalSettings']['metrika'] = $metrika;
  }
}
