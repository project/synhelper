<?php

namespace Drupal\synhelper\Hook;

/**
 * FileValidate.
 */
class FileValidate {

  /**
   * Hook.
   */
  public static function hook($file) {
    $fileSystem = \Drupal::service('file_system');
    if (!empty($file->destination) and $file->destination != NULL) {
      $destination = self::transliteration($file->destination);
      $basename = $fileSystem->basename($destination);
      $directory = $fileSystem->dirname($destination);
      $file->destination = $fileSystem->createFilename($basename, $directory);
    }
  }

  /**
   * Transliteration.
   */
  public static function transliteration($str) {
    $str = str_replace(' ', '_', $str);
    $str = \Drupal::transliteration()->transliterate($str);
    $str = strtolower($str);
    return $str;
  }

}
