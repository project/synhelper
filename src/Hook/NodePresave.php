<?php

namespace Drupal\synhelper\Hook;

/**
 * @file
 * Contains \Drupal\synhelper\Hook\NodePresave.
 */

/**
 * Controller NodePresave.
 */
class NodePresave {

  /**
   * Hook.
   */
  public static function hook($node) {
    // Set Node-title id empty.
    if (!$node->title->value) {
      $type = $node->getType();
      $date = \Drupal::service('date.formatter')->format(\Drupal::time()->getRequestTime(), 'long');
      $node->title->setValue("$type - $date");
    }
  }

}
