<?php

namespace Drupal\synhelper\Hook;

use Drupal\Core\Render\Markup;

/**
 * FormFieldUiFieldStorageAddFormAlter - Field Form Warning.
 */
class FormFieldUiFieldStorageAddFormAlter {

  /**
   * Hook.
   */
  public static function hook(&$form, &$form_state, $form_id) {
    $route = \Drupal::routeMatch()->getRouteName();
    $match = [
      'field_ui.field_storage_config_add_node',
      'field_ui.field_storage_config_add_taxonomy_term',
      'field_ui.field_storage_config_add_paragraph',
    ];
    if (in_array($route, $match)) {
      $path = \Drupal::request()->getRequestUri();
      $type = explode("/", $path)[5];
      if ($route == 'field_ui.field_storage_config_add_paragraph') {
        $type = explode("/", $path)[4];
      }
      $message = t("Use <code>'@type_'</code> prefix for field machine-name!
 Example: field_[<code>@type_position</code>] for Position field.<br>
<small>@class</small>", ['@class' => __CLASS__, '@type' => $type]);
      \Drupal::messenger()->addWarning(Markup::create($message));
    }
  }

}
