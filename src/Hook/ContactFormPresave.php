<?php

namespace Drupal\synhelper\Hook;

/**
 * @file
 * Contains \Drupal\synhelper\Hook\ContactFormPresave.
 */

use Drupal\contact\ContactFormInterface;

/**
 * Implements hook_ENTITY_TYPE_presave.
 */
class ContactFormPresave {

  /**
   * Hook.
   */
  public static function hook(ContactFormInterface $contact_form) {
    if (self::recipientsСhanged($contact_form)) {
      drupal_flush_all_caches();
    }
  }

  /**
   * Check.
   */
  private static function recipientsСhanged(ContactFormInterface $contact_form) {
    if (!$contact_form->original) {
      return TRUE;
    }
    return $contact_form->getRecipients() !== $contact_form->original->getRecipients();
  }

}
