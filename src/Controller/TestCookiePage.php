<?php

namespace Drupal\synhelper\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Setup test Cookie.
 */
class TestCookiePage extends ControllerBase {

  /**
   * Constructs page from template.
   */
  public function page() {
    $name = 'synhelper';
    $value = 'test';
    $host = "." . \Drupal::request()->getHost();
    $time = strtotime('+1 year');
    $cookie = new Cookie($name, $value, $time, '/', $host);
    $response = new JsonResponse([
      'cookie' => [$name => $value],
      'host' => $host,
      'data' => \Drupal::currentUser()->id(),
    ]);
    $response->headers->setCookie($cookie);
    return $response;
  }

}
