<?php

namespace Drupal\synhelper\Controller;

/**
 * @file
 * Contains \Drupal\app\Controller\AjaxResult.
 */

use Drupal\synhelper\Utility\AjaxResult as AjaxResultNew;

/**
 * Deprecated.
 */
class AjaxResult extends AjaxResultNew {

}
