<?php

namespace Drupal\migration;

use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Source for Plugins.
 */
class MigrationsSourceBase extends SourcePluginBase {

  //phpcs:disable
  protected bool $fetch = TRUE;
  protected bool $debug = TRUE;
  protected bool $uipage = FALSE;
  protected string $plugin_id;
  protected array $config = [];
  protected array $rows = [];
  //phpcs:enable

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    // SourcePlugin Settings.
    $this->plugin_id = $plugin_id;
    $this->trackChanges = TRUE;
    $this->config = self::accessProtected($migration, 'pluginDefinition')['process'];
    // Get Rows on Construct.
    if ($this->fetch) {
      $rows = $this->getRows();
      $this->rows = $rows;
    }
    // Debug for User if Is User Interface.
    if ($this->uiPage()) {
      $this->dsmDebug($plugin_id . ": ProcessMapping & Rows");
      $this->dsmDebug($this->config);
      $this->dsmDebug($rows);
    }
  }

  /**
   * Get Rows.
   */
  private function dsmDebug(mixed $rows) {
    if ($this->debug && \Drupal::moduleHandler()->moduleExists('devel')) {
      dsm($rows);
    }
  }

  /**
   * Get Rows.
   */
  public function getRows() {
    $rows = [];
    $this->rows = $rows;
    return $rows;
  }

  /**
   * UiPage.
   */
  public function uiPage() {
    $uipage = FALSE;
    if (\Drupal::routeMatch()->getRouteName() == "entity.migration.list") {
      $uipage = TRUE;
    }
    $this->uipage = $uipage;
    return $uipage;
  }

  /**
   * {@inheritdoc}
   */
  public static function accessProtected($obj, $prop) {
    $reflection = new \ReflectionClass($obj);
    $property = $reflection->getProperty($prop);
    $property->setAccessible(TRUE);
    return $property->getValue($obj);
  }

  /**
   * {@inheritdoc}
   */
  public function initializeIterator() {
    $rows = $this->getRows();
    return new \ArrayIterator($rows);
  }

  /**
   * Allows class to decide how it will react when it is treated like a string.
   */
  public function __toString() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getIDs() {
    return [
      'uuid' => [
        'type' => 'string',
        'alias' => 'id',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'uuid' => $this->t('UUID Key'),
      'id' => $this->t('ID Key'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function count($refresh = FALSE) {
    return count($this->rows);
  }

}
