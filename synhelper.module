<?php

/**
 * @file
 * Contains Hooks.
 */

use Drupal\synhelper\Hook\ModulesInstalled;
use Drupal\synhelper\Hook\ContactMessagePresave;
use Drupal\synhelper\Hook\PreprocessHtml;
use Drupal\synhelper\Hook\PreprocessPage;
use Drupal\synhelper\Hook\FileValidate;
use Drupal\synhelper\Hook\FormContactMailSettingsAlter;
use Drupal\synhelper\Hook\FormContactMessageFormAlter;
use Drupal\synhelper\Hook\FormAlter;
use Drupal\synhelper\Hook\FormMenuEditFormAlter;
use Drupal\synhelper\Hook\NodePresave;
use Drupal\synhelper\Hook\ContactFormPresave;
use Drupal\synhelper\Hook\CssAlter;
use Drupal\synhelper\Hook\PhpmailAlterFromAlter;
use Drupal\synhelper\Hook\FormUpdateManagerInstallFormAlter;
use Drupal\synhelper\Hook\FormFieldUiFieldStorageAddFormAlter;
use Drupal\synhelper\Hook\FormNodeTypeAddFormAlter;
use Drupal\synhelper\Hook\FormFieldConfigEditFormAlter;
use Drupal\synhelper\Hook\FormNodeFormAlter;
use Drupal\synhelper\Hook\FormCommerceCheckoutFlowMultistepDefaultAlter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\synhelper\Hook\Requirements;
use Drupal\contact\ContactFormInterface;

/**
 * Implements hook_requirements().
 */
function synhelper_requirements($phase) {
  return Requirements::hook($phase);
}


/**
 * Prepares variables for the page--service.html.twig template.
 */
function synhelper_preprocess_page__service(&$variables) {
  PreprocessPage::hook($variables);
}

/**
 * Prepares variables for the page.html.twig template.
 */
function synhelper_preprocess_page(&$variables) {
  PreprocessPage::hook($variables);
}

/**
 * Implements hook_modules_installed().
 */
function synhelper_modules_installed($modules) {
  ModulesInstalled::hook($modules);
}

/**
 * Implements hook_cmlexchange_orders_query_alter().
 */
function synhelper_phpmail_alter_from_alter(&$mail) {
  PhpmailAlterFromAlter::hook($mail);
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function synhelper_contact_form_presave(ContactFormInterface $contact_form) {
  ContactFormPresave::hook($contact_form);
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function synhelper_node_presave($entity) {
  NodePresave::hook($entity);
}

/**
 * Hook_css_alter() to embede CSS files as <link> elements.
 */
function synhelper_css_alter(&$css) {
  CssAlter::hook($css);
}

/**
 * Implements hook_form_FORM_ID_alter() for field_ui_field_storage_add_form().
 */
function synhelper_form_field_ui_field_storage_add_form_alter(&$form, $form_state, $form_id) {
  FormFieldUiFieldStorageAddFormAlter::hook($form, $form_state, $form_id);
}

/**
 * Implements hook_form_FORM_ID_alter() for node_type_add_form().
 */
function synhelper_form_node_type_add_form_alter(&$form, $form_state, $form_id) {
  FormNodeTypeAddFormAlter::hook($form, $form_state, $form_id);
}

/**
 * Implements hook_form_FORM_ID_alter() for field_config_edit_form().
 */
function synhelper_form_field_config_edit_form_alter(&$form, $form_state, $form_id) {
  FormFieldConfigEditFormAlter::hook($form, $form_state, $form_id);
}

/**
 * Implements hook_form_FORM_ID_alter() for node_page_form().
 */
function synhelper_form_update_manager_install_form_alter(&$form, $form_state, $form_id) {
  FormUpdateManagerInstallFormAlter::hook($form, $form_state, $form_id);
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function synhelper_contact_message_presave($entity) {
  ContactMessagePresave::setTitle($entity);
}


/**
 * Implements hook_preprocess_html().
 */
function synhelper_preprocess_html(&$variables) {
  PreprocessHtml::hook($variables);
}


/**
 * Implements hook_file_validate().
 */
function synhelper_file_validate($file) {
  FileValidate::hook($file);
}

/**
 * Implements hook_form_FORM_ID_alter() for menu_edit_form().
 */
function synhelper_form_menu_edit_form_alter(&$form, $form_state, $form_id) {
  FormMenuEditFormAlter::hook($form, $form_state, $form_id);
}

/**
 * Contact Message form alter.
 */
function synhelper_form_contact_message_form_alter(&$form, &$form_state, $form_id) {
  FormContactMessageFormAlter::hook($form, $form_state, $form_id);
}

/**
 * Contact Message form alter.
 */
function synhelper_form_alter(&$form, &$form_state, $form_id) {
  FormAlter::hook($form, $form_state, $form_id);
}

/**
 * Implements hook_form_alter().
 */
function synhelper_form_contact_mail_settings_alter(&$form, $form_state, $form_id) {
  FormContactMailSettingsAlter::hook($form, $form_state, $form_id);
}

/**
 * Implements hook_form_TYPE_alter().
 */
function synhelper_form_node_form_alter(&$form, $form_state, $form_id) {
  FormNodeFormAlter::hook($form, $form_state, $form_id);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function synhelper_form_commerce_checkout_flow_multistep_default_alter(&$form, FormStateInterface $form_state, $form_id) {
  FormCommerceCheckoutFlowMultistepDefaultAlter::hook($form, $form_state, $form_id);
}
